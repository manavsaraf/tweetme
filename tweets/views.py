from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic import DeleteView,ListView,DetailView,UpdateView,CreateView
from .models import Tweets
from .forms import TweetsForm
from .mixins import FormUserNeededMixin,UserOwnMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.views import View


class RetweetView(View):
    def get(self, request, pk, *args, **kwargs):
        tweet = get_object_or_404(Tweets, pk=pk)
        if request.user.is_authenticated:
            new_tweet = Tweets.objects.retweet(request.user, tweet)
            return HttpResponseRedirect("/tweets/")
        return HttpResponseRedirect(tweet.get_absolute_url())

class TweetsCreate(FormUserNeededMixin,CreateView):

    model=Tweets
    form_class=TweetsForm
    success_url=reverse_lazy('tweets:list')

class TweetsUpdate(UserOwnMixin,UpdateView):
    model=Tweets
    fields=['content']
    template_name='tweets/tweets_update.html'


class TweetsList(LoginRequiredMixin,ListView):
    model=Tweets
    def get_queryset(self,*args,**kwargs):
        qs=Tweets.objects.all()
        query=self.request.GET.get("q",None)
        if query  is not None:
            qs=qs.filter(Q(content__icontains=query) |
                         Q(user__username__icontains=query)
                         )

        return qs
    def get_context_data(self,*args,**kwargs):
        context=super(TweetsList,self).get_context_data(*args,**kwargs)
        context['create_form']=TweetsForm()
        context['create_url']=reverse_lazy('tweets:create')
        return context

class TweetsDetail(UserOwnMixin,DetailView):
    model=Tweets

class TweetsDelete(UserOwnMixin,DeleteView):
    model=Tweets
    success_url=reverse_lazy('tweets:list')
