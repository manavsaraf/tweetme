from django.forms.utils import ErrorList
from django import forms
class FormUserNeededMixin(object):
    def form_valid(self,form):
        if self.request.user.is_authenticated:
            form.instance.user=self.request.user
            return super(FormUserNeededMixin,self).form_valid(form)
        else:
            form._errors[forms.forms.NON_FIELD_ERRORS]=ErrorList(["user must be logged in to continue"])
            return self.form_invalid(form)

class UserOwnMixin(FormUserNeededMixin,object):
    def form_valid(self,form):
        if form.instance.user==self.request.user:
                return super(FormUserNeededMixin,self).form_valid(form)
        else:
                form._errors[forms.forms.NON_FIELD_ERRORS]=ErrorList(["you are not the appropriate user to update it"])
                return self.form_invalid(form)
