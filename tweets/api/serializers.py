from tweets.models import Tweets
from django.utils.timesince import timesince
from rest_framework import serializers
from accounts.api.serializers import UserSerializerDisplay


class ParentTweetsSerializer(serializers.ModelSerializer):
    user=UserSerializerDisplay(read_only=True)
    date_display=serializers.SerializerMethodField()
    timesince=serializers.SerializerMethodField()
    likes=serializers.SerializerMethodField()
    did_like=serializers.SerializerMethodField()
    class Meta:
        model=Tweets
        fields=['id','user','content','timestamp','date_display','timesince','likes','did_like']

    def get_likes(self,obj):
        return obj.liked.all().count()
    def get_date_display(self,obj):
        return obj.timestamp.strftime("%b %d, %Y %I: %M %p")
    def get_timesince(self,obj):
        return timesince(obj.timestamp)+"ago"

    def get_did_like(self,obj):
        try:
            user=request.user
            if user.is_authenticated:
                if user in obj.liked.all():
                   return True
        except:
            pass
        return False
class TweetsSerializer(serializers.ModelSerializer):
    parent_id=serializers.CharField(write_only=True,required=False)
    user=UserSerializerDisplay(read_only=True)
    date_display=serializers.SerializerMethodField()
    timesince=serializers.SerializerMethodField()
    likes=serializers.SerializerMethodField()
    did_like=serializers.SerializerMethodField()
    parent=ParentTweetsSerializer(read_only=True)

    class Meta:
        model=Tweets
        fields=['parent_id','id','user','content','timestamp','date_display','timesince','parent','likes','did_like','reply']
    def get_did_like(self,obj):
        request=self.context.get("request")
        try:
            user=request.user
            if user.is_authenticated:
                if user in obj.liked.all():
                    return True
        except:
            pass
        return False
    def get_likes(self,obj):
        return obj.liked.all().count()

    def get_date_display(self,obj):
        return obj.timestamp.strftime("%b %d, %Y %I: %M %p")
    def get_timesince(self,obj):
        return timesince(obj.timestamp)+"ago"
