from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from .views import TweetsListApiView,TweetsCreateApiView,RetweetAPIView,LikeToggleAPIView,TweetsDetailApiView
urlpatterns = [

       url(r'^$',TweetsListApiView.as_view(),name="api-list"),
       url(r'create/',TweetsCreateApiView.as_view(),name="api-create"),
       url(r'^(?P<pk>\d+)/retweet/$',RetweetAPIView.as_view(),name="retweet"),
       url(r'^(?P<pk>\d+)/$',TweetsDetailApiView.as_view(),name="api-detail"),
       url(r'^(?P<pk>\d+)/like/$',LikeToggleAPIView.as_view(),name="like-toggle"),

]

if settings.DEBUG:
    urlpatterns += (static(settings.STATIC_URL,document_root=settings.STATIC_ROOT))
