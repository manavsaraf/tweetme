from django.contrib import admin
from .models import Tweets
from .forms import TweetsForm


class AdminForm(admin.ModelAdmin):
    #form=TweetsForm
    class Meta:
        model=Tweets
admin.site.register(Tweets,AdminForm)
