from django.conf.urls import url
from . import views

app_name='tweets'
urlpatterns=[
    url(r'^$',views.TweetsList.as_view(),name="list"),
    url(r'create',views.TweetsCreate.as_view(),name="create"),
    url(r'^(?P<pk>\d+)/$',views.TweetsDetail.as_view(),name="detail"),
    url(r'^(?P<pk>\d+)/retweet/$',views.RetweetView.as_view(),name="retweet"),
    url(r'^(?P<pk>\d+)/update/$',views.TweetsUpdate.as_view(),name="update"),
    url(r'^(?P<pk>\d+)/delete/$',views.TweetsDelete.as_view(),name="Delete"),
]
