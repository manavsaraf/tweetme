from django.contrib.auth import get_user_model
from .models import Tweets
from django.test import TestCase
from django.urls import reverse

User=get_user_model()
class TweetsTest(TestCase):
    def setUp(self):
        some_user=User.objects.create(username='chottu')

    def test_tweets_item(self):
        obj=Tweets.objects.create(
                user=User.objects.first(),
                content='some content'
                )

        self.assertTrue(obj.content=='some content')
        self.assertTrue(obj.id==1)
        abolute_url=reverse('tweets:detail',kwargs={'pk':1})
        self.assertEqual(obj.get_absolute_url(),abolute_url)

    def test_tweets_url(self):
        obj=Tweets.objects.create(
                user=User.objects.first(),
                content='some content'
                )

        abolute_url=reverse('tweets:detail',kwargs={'pk':obj.pk})
        self.assertEqual(obj.get_absolute_url(),abolute_url)
