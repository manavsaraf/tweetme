from .models import Tweets
from django import forms

class TweetsForm(forms.ModelForm):
    content=forms.CharField(label='',widget=forms.Textarea(attrs={'placeholder':'Your Message',"class":"form-control"}))
    class Meta:
        model=Tweets
        fields=['content']
