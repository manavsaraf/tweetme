"""tweetme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from . import views
from tweets.api.views import SearchTweetApiView
from hashtags.views import HashTagView
from .views import SearchView
from hashtags.api.views import TagTweetAPIView
from accounts.views import UserRegisterView
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$',views.home,name="home"),
    url(r'^search/$',SearchView.as_view(),name="search"),
    url(r'^register/$',UserRegisterView.as_view(),name="register"),
    url(r'^api/search/$',SearchTweetApiView.as_view(),name="api-search"),
    url(r'^api/tags/(?P<hashtag>.*)/$',TagTweetAPIView.as_view(),name="api-tag"),
    url(r'^tweets/',include('tweets.urls')),
    url(r'^tags/(?P<hashtag>.*)/$',HashTagView.as_view(),name="hashtag"),
    url(r'api/tweets/',include('tweets.api.urls')),
    url(r'api/',include('accounts.api.urls')),
    url(r'^',include('django.contrib.auth.urls')),
    url(r'^',include('accounts.urls')),

]

if settings.DEBUG:
    urlpatterns += (static(settings.STATIC_URL,document_root=settings.STATIC_ROOT))
